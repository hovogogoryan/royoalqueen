<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $blogs = Blog::all();
            return view('admin.blog')->with('blog', $blogs);
        }catch (\Exception $err){
            return $err;
        }
    }

    /**
     *
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)


    {

        try{
            $file = $request->file('image');
            $avatar = rand(11111, 99999) .time(). '.' . $file->getClientOriginalExtension();
            $request->file('image')->move("image", $avatar);
            $blogs = new Blog();
            $blogs->title = $request->title;
            $blogs->content = $request->description;
            $blogs->image = 'image/'.$avatar;
            $blogs->save();
            return response(['message'=>'Հաջողությամբ ավելացվել է', 'status'=>1, 'blog'=>$blogs]);
        }catch(\Exception $err){
            return $err;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            Blog::where('id',$id)->update(['title'=>$request->title,"content"=>$request->description]);
            return response(['message'=>'Հաջողությամբ փոփոխվել է']);
        }catch (\Exception $err){
            return $err;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            Blog::where('id',$id)->delete();
            return response(['message'=>'Հաջողությամբ ջնջվել է']);
        }catch (\Exception $err){
            return $err;
        }
    }
}
