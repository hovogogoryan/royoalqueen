<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     try{
         $categories = Category::all();

         return view('admin.category')->with('categories', $categories);
     }catch (\Exception $err){
         return $err;
     }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryRequest $request)
    {
        try{
            $category = Category::create($request->all());
            return response(['message'=>'Հաջողությամբ ավելացվել է', 'status'=>1, 'category'=>$category]);
        }catch(\Exception $err){
            return $err;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $products = Product::where("category_id", $id)->paginate(8);
        return view('shop',compact('products'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       try{
           Category::where('id',$id)->update(['name'=>$request->name]);
           return response(['message'=>'Հաջողությամբ փոփոխվել է']);
       }catch (\Exception $err){
           return $err;
       }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            Category::where('id',$id)->delete();
            return response(['message'=>'Հաջողությամբ ջնջվել է']);
        }catch (\Exception $err){
            return $err;
        }
    }
}
