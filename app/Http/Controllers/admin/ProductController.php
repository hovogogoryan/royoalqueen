<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      try{
          $products = Product::all();

          return view('admin.product')->with('products', $products);
      }catch (\Exception $err){
          return $err;
      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = $request->file('avatar');
        $avatar = rand(11111, 99999) .time(). '.' . $file->getClientOriginalExtension();
        $request->file('avatar')->move("images", $avatar);

        $input=$request->all();
        $images=array();
        if($files=$request->file('images')){
            $i=0;
            foreach($files as $file){
                $i++;
                $name=$i.time().$file->getClientOriginalName();
                $file->move('images',$name);
                $images[]='images/'.$name;
            }
        }

//
//        $request['avatar']=$avatar;
//        $request['images']=json_encode($images);
        try {
            $products = new Product();
            $products->name = $request->name;
            $products->price = $request->price;
            $products->cost = $request->cost;
            $products->discount = $request->discount;
            $products->description = $request->description;
            $products->avatar = 'images/'.$avatar;
            $products->images = json_encode($images);
            $products->category_id = $request->category_id;
            $products->see_count = $request->see_count;
            $products->save();
            return response(['message' => 'Հաջողությամբ ավելացվել է', 'status' => 1, 'products' => $products]);
        } catch (\Exception $err) {
            return $err;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      try{
          Product::where('id',$id)->update(
              [
                  'name'=>$request->name,
                  'cost'=>$request->cost,
                  'price'=>$request->price,
                  'description'=>$request->description,
                  'category_id'=>$request->category_id,
                  'see_count'=>$request->see_count,
              ]);
          return response(['message'=>'Հաջողությամբ փոփոխվել է']);
      }catch (\Exception $err){
          return $err;
      }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Product::where('id', $id)->delete();
            return response(['message' => 'Հաջողությամբ ջնջվել է']);
        } catch (\Exception $err) {
            return $err;
        }
    }
}
