<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Slider;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
        $sliders = Slider::all();
        return view('admin.slider')->with('sliders', $sliders);
    }catch (\Exception $err){
        return $err;
    }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $file = $request->file('image');
            $avatar = rand(11111, 99999) .time(). '.' . $file->getClientOriginalExtension();
            $request->file('image')->move("images", $avatar);

            $sliders = new Slider();
            $sliders->title = $request->title;
            $sliders->image = 'images/'.$avatar;
            $sliders->save();
            return response(['message'=>'Հաջողությամբ ավելացվել է', 'status'=>1, 'sliders'=>$sliders]);
        }catch(\Exception $err){
            return $err;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            Slider::where('id',$id)->update(['title'=>$request->name]);
            return response(['message'=>'Հաջողությամբ փոփոխվել է']);
        }catch (\Exception $err){
            return $err;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            Slider::where('id',$id)->delete();
            return response(['message'=>'Հաջողությամբ ջնջվել է']);
        }catch (\Exception $err){
            return $err;
        }
    }
}
