<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $fillable=[
        'name', 'cost', 'description', 'price', 'avatar','images',"category_id","see_count","discount"
    ];
    function category(){
        return $this->belongsTo('App\Models\Category');
    }
}
