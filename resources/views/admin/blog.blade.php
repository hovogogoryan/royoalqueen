@extends('admin.layouts.app')
@section('content')

    <div class="container content-wrapper">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Add Blog</h3>
            </div>
            <form enctype="multipart/form-data" id="upload_form_blog">
                @csrf
                <div class="container">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Title</label>
                        <input type="text" class="form-control" id="blog-title" placeholder="Title" name="title">
                    </div>
                </div>


                <div class="container">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Image</label>
                        <input type="file" class="form-control"  placeholder="Image" name="image">
                    </div>
                </div>


                <div class="container">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Description</label>
                        <input type="text" class="form-control" id="blog-description" placeholder="Description" name="description">
                    </div>
                </div>

                <div class="card-footer">
                    <button type="submit" id="add-blog" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">

                            <div class="card-body">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>TITLE</th>
                                        <th>DESCRIPTION</th>
                                        <th>IMAGE</th>
                                        <th>CREATED_AT</th>
                                        <th>UPDATED_AT</th>
                                        <th>DELETE</th>
                                        <th>EDIT</th>
                                    </tr>
                                    </thead>
                                    <tbody  id="blog-tbody">
                                    @foreach($blog as $key)
                                        <tr>
                                            <td>{{ $key['id'] }}</td>
                                            <td><input  type="text" value="{{ $key['title'] }}" class="title"></td>
                                            <td><input  type="text" value="{{ $key['content'] }}" class="content"></td>
                                            <td><img src="{{ asset($key['image']) }}" style="width:100px" alt=""></td>
                                            <td><input  type="text" value="{{ $key['created_at'] }}" class="created_at"></td>
                                            <td><input  type="text" value="{{ $key['updated_at'] }}" class="updated_at"></td>
                                            <td><button class="delete-blog btn btn-danger">Delete</button></td>
                                            <td><button class="edit-blog btn btn-info">Edit</button></td>
                                        </tr>
                                    @endforeach
                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>

@endsection
