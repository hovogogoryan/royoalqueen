@extends('admin.layouts.app')
@section('content')

    <div class="container content-wrapper">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Add Brand</h3>
            </div>
            <form enctype="multipart/form-data" id="upload_form_brand">
                @csrf
                <div class="card-body">

                    <div class="form-group">
                        <label for="exampleInputEmail1">IMAGES</label>
                        <input type="file" class="form-control" name="image" multiple  placeholder="image">
                    </div>

                    <div class="card-footer">
                        <button type="submit" id="add-brand" class="btn btn-primary">Submit</button>
                    </div>
                </div>


            </form>
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>IMAGE</th>
                                    <th>CREATED_AT</th>
                                    <th>UPDATED_AT</th>
                                    <th>Delete</th>
                                    <th>Edit</th>
                                </tr>
                                </thead>
                                <tbody id="brand-tbody">
                                @foreach($brand as $key)
                                    <tr>
                                        <td>{{ $key['id'] }}</td>
                                        <td><img src="{{ asset($key['image']) }}" style="width:100px" alt=""></td>
                                        <td><input  type="text" value="{{ $key['created_at'] }}" class="created_at"></td>
                                        <td><input  type="text" value="{{ $key['updated_at'] }}" class="updated_at"></td>
                                        <td><button class="delete-brand btn btn-danger">Delete</button></td>
                                        <td><button class="edit-brand btn btn-info">Edit</button></td>
                                    </tr>
                                @endforeach
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>

@endsection
