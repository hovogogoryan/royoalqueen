@extends('admin.layouts.app')
@section('content')

    <div class="container content-wrapper">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Add Category</h3>
            </div>
            <form>
                @csrf
                <div class="container">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Name</label>
                        <input type="text" class="form-control" id="category-name" placeholder="Name">
                    </div>
                </div>

                <div class="card-footer">
                    <button type="button" id="add-category" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">

                            <div class="card-body">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>NAME</th>
                                        <th>CREATED_AT</th>
                                        <th>UPDATED_AT</th>
                                        <th>DELETE</th>
                                        <th>EDIT</th>
                                    </tr>
                                    </thead>
                                    <tbody  id="category-tbody">
                                                @foreach($categories as $key)
                                           <tr>
                                               <td>{{ $key['id'] }}</td>
                                               <td><input  type="text" value="{{ $key['name'] }}" class="name-category"></td>
                                               <td>{{ $key['created_at'] }}</td>
                                               <td>{{ $key['updated_at'] }}</td>

                                               <td><button class="delete-category btn btn-danger">Delete</button></td>
                                               <td><button class="edit-category btn btn-success">Edit</button></td>
                                           </tr>
                                                @endforeach
                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>

@endsection
