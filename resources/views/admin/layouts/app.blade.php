<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title></title>

    <link rel="stylesheet" href="{{ asset('adminResource/plugins/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet"
          href="{{ asset('adminResource/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('adminResource/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('adminResource/plugins/jqvmap/jqvmap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('adminResource/dist/css/adminlte.min.css') }}">
    <link rel="stylesheet" href="{{ asset('adminResource/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
    <link rel="stylesheet" href="{{ asset('adminResource/plugins/daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('adminResource/plugins/summernote/summernote-bs4.min.css') }}">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

    <div class="preloader flex-column justify-content-center align-items-center">
        <img class="animation__shake" src="{{ asset('adminResource/dist/img/AdminLTELogo.png') }}" alt="AdminLTELogo"
             height="60" width="60">
    </div>

    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="index3.html" class="nav-link">Home</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="#" class="nav-link">Contact</a>
            </li>
        </ul>

        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" data-widget="navbar-search" href="#" role="button">
                    <i class="fas fa-search"></i>
                </a>
                <div class="navbar-search-block">
                    <form  action="/search" method="POST"  class="form-inline">
                        <div class="input-group input-group-sm">
                            <input class="form-control form-control-navbar" type="search" placeholder="Search"
                                   aria-label="Search">
                            <div class="input-group-append">
                                <button class="btn btn-navbar" type="submit">
                                    <i class="fas fa-search"></i>
                                </button>
                                <button class="btn btn-navbar" type="button" data-widget="navbar-search">
                                    <i class="fas fa-times"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </li>

            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                    <i class="far fa-comments"></i>
                    <span class="badge badge-danger navbar-badge">3</span>
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    <a href="#" class="dropdown-item">
                        <!-- Message Start -->
                        <div class="media">
                            <img src="{{ asset('adminResource/dist/img/user1-128x128.jpg') }}" alt="User Avatar"
                                 class="img-size-50 mr-3 img-circle">
                            <div class="media-body">
                                <h3 class="dropdown-item-title">
                                    Brad Diesel
                                    <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                                </h3>
                                <p class="text-sm">Call me whenever you can...</p>
                                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                            </div>
                        </div>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <div class="media">
                            <img src="{{ asset('adminResource/dist/img/user8-128x128.jpg') }}" alt="User Avatar"
                                 class="img-size-50 img-circle mr-3">
                            <div class="media-body">
                                <h3 class="dropdown-item-title">
                                    John Pierce
                                    <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
                                </h3>
                                <p class="text-sm">I got your message bro</p>
                                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                            </div>
                        </div>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <div class="media">
                            <img src="adminResource/dist/img/user3-128x128.jpg" alt="User Avatar"
                                 class="img-size-50 img-circle mr-3">
                            <div class="media-body">
                                <h3 class="dropdown-item-title">
                                    Nora Silvester
                                    <span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>
                                </h3>
                                <p class="text-sm">The subject goes here</p>
                                <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                            </div>
                        </div>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
                </div>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                    <i class="far fa-bell"></i>
                    <span class="badge badge-warning navbar-badge">15</span>
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    <span class="dropdown-item dropdown-header">15 Notifications</span>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <i class="fas fa-envelope mr-2"></i> 4 new messages
                        <span class="float-right text-muted text-sm">3 mins</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <i class="fas fa-users mr-2"></i> 8 friend requests
                        <span class="float-right text-muted text-sm">12 hours</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item">
                        <i class="fas fa-file mr-2"></i> 3 new reports
                        <span class="float-right text-muted text-sm">2 days</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                    <i class="fas fa-expand-arrows-alt"></i>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
                    <i class="fas fa-th-large"></i>
                </a>
            </li>
        </ul>
    </nav>

    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="index3.html" class="brand-link">
            <img src="{{ asset('adminResource/dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo"
                 class="brand-image img-circle elevation-3" style="opacity: .8">
            <span class="brand-text font-weight-light">Royal_Queen_Roses</span>
        </a>

        <div class="sidebar">
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
                <div class="image">
                    <img src="{{ asset('adminResource/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2"
                         alt="User Image">
                </div>
                <div class="info">
                    <a href="#" class="d-block">777777</a>
                </div>
            </div>

            <div class="form-inline">
                <div class="input-group" data-widget="sidebar-search">
                    <input class="form-control form-control-sidebar" type="search" placeholder="Search"
                           aria-label="Search">
                    <div class="input-group-append">
                        <button class="btn btn-sidebar">
                            <i class="fas fa-search fa-fw"></i>
                        </button>
                    </div>
                </div>
            </div>

            <nav class="mt-2">
                <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                    data-accordion="false">
                    <li class="nav-item">
                        <a href="{{ asset('/admin/category') }}" class="nav-link">
                            <i class="nav-icon fas fa-th"></i>
                            <p>
                                Category
                            </p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{ asset('/admin/product') }}" class="nav-link">
                            <i class="nav-icon fas fa-th"></i>
                            <p>
                                Product
                            </p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{ asset('/admin/slider') }}" class="nav-link">
                            <i class="nav-icon fas fa-th"></i>
                            <p>
                                Slider
                            </p>
                        </a>
                    </li>


                    <li class="nav-item">
                        <a href="{{ asset('/admin/review') }}" class="nav-link">
                            <i class="nav-icon fas fa-th"></i>
                            <p>
                                Review
                            </p>
                        </a>
                    </li>

                    <li class="nav-item">
                        <a href="{{ asset('/admin/blog') }}" class="nav-link">
                            <i class="nav-icon fas fa-th"></i>
                            <p>
                                Blog
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ asset('/admin/brand') }}" class="nav-link">
                            <i class="nav-icon fas fa-th"></i>
                            <p>
                                Brand
                            </p>
                        </a>
                    </li>
                    <li class="nav-item">
                            <a class="nav-link" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                <i class="nav-icon fas fa-th"></i>
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                    </li>
                </ul>
            </nav>
        </div>

    </aside>

    <div class="content">

        @yield('content')
    </div>

    <aside class="control-sidebar control-sidebar-dark">
    </aside>
</div>

<script src="{{ asset('adminResource/plugins/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('adminResource/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<script>
    $.widget.bridge('uibutton', $.ui.button)
</script>
<script src="{{ asset('adminResource/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('adminResource/plugins/chart.js/Chart.min.js') }}"></script>
<script src="{{ asset('adminResource/plugins/sparklines/sparkline.js') }}"></script>
<script src="{{ asset('adminResource/plugins/jqvmap/jquery.vmap.min.js') }}"></script>
<script src="{{ asset('adminResource/plugins/jqvmap/maps/jquery.vmap.usa.js') }}"></script>
<script src="{{ asset('adminResource/plugins/jquery-knob/jquery.knob.min.js') }}"></script>
<script src="{{ asset('adminResource/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('adminResource/plugins/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('adminResource/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<script src="{{ asset('adminResource/plugins/summernote/summernote-bs4.min.js') }}"></script>
<script src="{{ asset('adminResource/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<script src="{{ asset('adminResource/dist/js/adminlte.js') }}"></script>
<script src="{{ asset('adminResource/dist/js/demo.js') }}"></script>
<script src="{{ asset('adminResource/dist/js/pages/dashboard.js') }}"></script>


</body>
</html>




<script>
    let siteUrl = "http://127.0.0.1:8000/";

    function deleteTr() {
        $('.delete-category').click(function () {
            let id = $(this).parent().parent().find('td:first-child').text();
            $(this).parent().parent().remove();
            $.ajax({
                type: "GET",
                cache: false,
                url: `/admin/category/delete/` + id,
                success: function (response) {

                }
            });
        })
    }


    function deleteBrand() {
        $('.delete-brand').click(function () {
            let id = $(this).parent().parent().find('td:first-child').text();
            $(this).parent().parent().remove();
            $.ajax({
                type: "GET",
                cache: false,
                url: `/admin/brand/delete/` + id,
                success: function (response) {

                }
            });
        })
    }
    deleteBrand()




    function deleteBl() {
        $('.delete-blog').click(function () {
            let id = $(this).parent().parent().find('td:first-child').text();
            $(this).parent().parent().remove();
            $.ajax({
                type: "GET",
                cache: false,
                url: `/admin/blog/delete/` + id,
                success: function (response) {

                }
            });
        })
    }

    deleteBl();


    function deleteSl() {
        $('.delete-slider').click(function () {
            let id = $(this).parent().parent().find('td:first-child').text();
            $(this).parent().parent().remove();
            $.ajax({
                type: "GET",
                cache: false,
                url: `/admin/slider/delete/` + id,
                success: function (response) {

                }
            });
        })
    }

    function deletePr() {
        $('.delete-product').click(function () {
            let id = $(this).parent().parent().find('td:first-child').text();
            $(this).parent().parent().remove();
            $.ajax({
                type: "GET",
                cache: false,
                url: `/admin/product/delete/` + id,
                success: function (response) {

                }
            });
        })
    }

    $("#add-category").click(function () {
        $name = $("#category-name").val();
        let data = {_token: '{{ csrf_token() }}', name: $name}
        $.ajax({
            type: "POST",
            cache: false,
            url: "/admin/category/store",
            data: data,    // multiple data sent using ajax
            success: function (response) {
                $('#category-tbody').append(`
                         <tr>
                            <td>${response.category.id}</td>
                             <td>${response.category.name} </td>
                             <td>${response.category.created_at}</td>
                             <td>${response.category.updated_at}</td>
                             <td><button class=' btn btn-danger delete-category'>Delete</button></td>
                             <td><button class='btn btn-success edit-category' >Edit</button></td>

                         </tr>
               `);
                deleteTr()
            }
        });
    })



    $("#add-review").click(function () {
        $name = $("#review-name").val();
        $email = $("#review-email").val();
        $status = $("#review-status").val();
        $product_id = $("#review-product_id").val();
        let data = {_token: '{{ csrf_token() }}', name: $name,}
        $.ajax({
            type: "POST",
            cache: false,
            url: "/admin/review/store",
            data: data,    // multiple data sent using ajax
            enctype: 'multipart/form-data',
            success: function (response) {
                $('#review-tbody').append(`
                         <tr>
                            <td>${response.review.id}</td>
                             <td>${response.review.name} </td>
                             <td>${response.review.email} </td>
                             <td>${response.review.status} </td>
                             <td>${response.review.product_id} </td>

                             <td><button class=' btn btn-danger delete-review'>Delete</button></td>
                             <td><button class='btn btn-success edit-review'>Edit</button></td>

                         </tr>
               `);

            }
        });
    })








    $('#upload_form_product').on('submit', function (event) {
        event.preventDefault();
        $.ajax({
            url: "/admin/product/store",
            method: "POST",
            data: new FormData(this),
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            enctype: 'multipart/form-data',

            success: function (response) {
                $('#product-tbody').append(`
                         <tr>
                            <td>${response.products.id}</td>
                             <td>${response.products.name} </td>
                             <td>${response.products.description}</td>
                             <td>${response.products.cost}</td>
                             <td>${response.products.price}</td>
                             <td>${response.products.discount}</td>
                             <td><img src="${siteUrl + response.products.avatar}" style="width:100px ;" alt=""></td>
                             <td>${response.products.category_id}</td>
                             <td>${response.products.see_count}</td>
                             <td><button class='delete-product btn btn-danger' >Delete</button></td>
                         </tr>

`);
                deletePr();

            }

        })
    })



    $('#upload_form_blog').on('submit', function (event) {
        event.preventDefault();
        $.ajax({
            url: "/admin/blog/store",
            method: "POST",
            data: new FormData(this),
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            enctype: 'multipart/form-data',

            success: function (response) {
                $('#blog-tbody').append(`
                         <tr>
                            <td>${response.blog.id}</td>
                            <td>${response.blog.title} </td>
                            <td>${response.blog.content}</td>
                            <td><img src="${siteUrl + response.blog.image}" style="width:100px ;" alt=""></td>
                            <td>${response.blog.created_at} </td>
                            <td>${response.blog.updated_at} </td>
                            <td><button class='delete-blog btn btn-danger' >Delete</button></td>
                            <td><button class='edit-blog btn btn-success' >Edit</button></td>
                         </tr>

               `);
            }
        })
    })





    $('#upload_form_slider').on('submit', function (event) {
        event.preventDefault();
        $.ajax({
            url: "/admin/slider/store",
            method: "POST",
            data: new FormData(this),
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            enctype: 'multipart/form-data',

            success: function (response) {
                $('#slider-tbody').append(`
                         <tr>
                            <td>${response.sliders.id}</td>
                             <td>${response.sliders.title} </td>
                             <td><img src="${siteUrl + response.sliders.image}" style="width:100px ;" alt=""></td>
                             <td><button class='delete-slider btn btn-danger' >Delete</button></td>
                         </tr>

               `)
                deleteSl()
            }
        })
    })

    $('#upload_form_brand').on('submit', function (event) {
        event.preventDefault();
        $.ajax({
            url: "/admin/brand/store",
            method: "POST",
            data: new FormData(this),
            dataType: 'JSON',
            contentType: false,
            cache: false,
            processData: false,
            enctype: 'multipart/form-data',
            success: function (response) {
                $('#brand-tbody').append(`
                         <tr>
                             <td>${response.brand.id}</td>
                             <td><img src="${siteUrl + response.brand.image}" style="width:100px ;" alt=""></td>
                             <td>${response.brand.created_at} </td>
                             <td>${response.brand.updated_at} </td>

                             <td><button class='delete-brand btn btn-danger' >Delete</button></td>
                             <td><button class='edit-brand btn btn-success' >Edit</button></td>
                         </tr>

               `)
            }
        })
    })


    deleteSl()
    deleteTr();
    deletePr()
    $('.name-category').keyup(function (e) {
        let id = $(this).parent().parent().find('td:first-child').text();
        _this = $(this);
        $name = $(this).parent().parent().find('.name').val();

        if (e.keyCode == 13) {
            $.ajax({
                type: "POST",
                cache: false,
                url: `/admin/category/update/` + id,
                data: {
                    _token: '{{csrf_token()}}',
                    name: _this.val(),
                    name: $name,

                },

                success: function (response) {
                    _this.css({'border': '5px solid green'})
                    setTimeout(function () {
                        _this.css({'border': '1px solid black'})
                    }, 2000)
                }
            });
        }
    });


    $('.edit-category').click(function (e) {
        let id = $(this).parent().parent().find('td:first-child').text();
        _this = $(this);
        $name = $(this).parent().parent().find('.name').val();

        $.ajax({
            type: "POST",
            cache: false,
            url: `/admin/category/update/` + id,
            data: {
                _token: '{{csrf_token()}}',
                name: $name,
            },
            success: function (response) {
                _this.css({'border': '5px solid green'})
                setTimeout(function () {
                    _this.css({'border': '1px solid black'})
                }, 2000)
            }
        });
    });




    $('.edit-product').click(function (e) {
        let id = $(this).parent().parent().find('td:first-child').text();
        _this = $(this);
        $name = $(this).parent().parent().find('.name').val();
        $cost = $(this).parent().parent().find('.cost').val();
        $description = $(this).parent().parent().find('.description').val();
        $price = $(this).parent().parent().find('.price').val();
        $see_count = $(this).parent().parent().find('.see_count').val();
        $category_id = $(this).parent().parent().find('.category_id').val();
        $.ajax({
            type: "POST",
            cache: false,
            url: `/admin/product/update/` + id,
            data: {
                _token: '{{csrf_token()}}',
                name: $name,
                price: $price,
                cost: $cost,
                description: $description,
                see_count: $see_count,
                category_id: $category_id,
            },
            success: function (response) {
                _this.css({'border': '5px solid green'})
                setTimeout(function () {
                    _this.css({'border': '1px solid black'})
                }, 2000)
            }
        });
    });



    $('.edit-slider').click(function (e) {
        let id = $(this).parent().parent().find('td:first-child').text();
        _this = $(this);
        $name = $(this).parent().parent().find('.name').val();

        $.ajax({
            type: "POST",
            cache: false,
            url: `/admin/slider/update/` + id,
            data: {
                _token: '{{csrf_token()}}',
                name: $name,
            },
            success: function (response) {
                _this.css({'border': '5px solid green'})
                setTimeout(function () {
                    _this.css({'border': '1px solid black'})
                }, 2000)
            }
        });
    });






    $('.edit-blog').click(function (e) {
        let id = $(this).parent().parent().find('td:first-child').text();
        _this = $(this);
        $title = $(this).parent().parent().find('.title').val();
        $content = $(this).parent().parent().find('.content').val();

        $.ajax({
            type: "POST",
            cache: false,
            url: `/admin/blog/update/` + id,
            data: {
                _token: '{{csrf_token()}}',
                title: $title,
                description: $content,

            },
            success: function (response) {
                _this.css({'border': '5px solid green'})
                setTimeout(function () {
                    _this.css({'border': '1px solid black'})
                }, 2000)
            }
        });
    });







    $('.edit-brand').click(function (e) {
        let id = $(this).parent().parent().find('td:first-child').text();
        _this = $(this);
        $image = $(this).parent().parent().find('.image').val();


        $.ajax({
            type: "POST",
            cache: false,
            url: `/admin/brand/update/` + id,
            data: {
                _token: '{{csrf_token()}}',
                image: $image,

            },
            success: function (response) {
                _this.css({'border': '5px solid green'})
                setTimeout(function () {
                    _this.css({'border': '1px solid black'})
                }, 2000)
            }
        });
    });


</script>
