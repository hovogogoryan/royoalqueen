@extends('admin.layouts.app')
@section('content')

    <div class="container content-wrapper">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Add Product</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form enctype="multipart/form-data" id="upload_form_product">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">NAME</label>
                        <input type="text" class="form-control" name="name" placeholder="NAME">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">DESCRIPTION</label>
                        <input type="text" class="form-control" name="description" placeholder="DESCRIPTION">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">COST</label>
                        <input type="text" class="form-control" name="cost" placeholder="COST">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">PRICE</label>
                        <input type="text" class="form-control" name="price" placeholder="PRICE">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Discount</label>
                        <input type="text" class="form-control" name="discount" placeholder="Discount">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">AVATAR</label>
                        <input type="file" class="form-control" name="avatar"
                               placeholder="AVATAR">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">IMAGES</label>
                        <input type="file" class="form-control" name="images[]" multiple
                               placeholder="IMAGES">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">CATEGORY_ID</label>
                        <select name="category_id" class="form-control" id="">
                            @foreach($categories as $key)
                                <option value="{{ $key->id }}">{{ $key->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">SEE_COUNT</label>
                        <input type="text" class="form-control" name="see_count"
                               placeholder="SEE_COUNT">
                    </div>
                    <div class="card-footer">
                        <button type="submit" id="add-product" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <table id="example2" class="table table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Description</th>
                                    <th>Cost</th>
                                    <th>Price</th>
                                    <th>Avatar</th>
                                    <th>See_count</th>
                                    <th>Discount</th>
                                    <th>Delete</th>
                                    <th>Edit</th>
                                </tr>
                                </thead>
                                <tbody id="product-tbody">
                                @foreach($products as $key)
                                    <tr>
                                        <td>{{ $key['id'] }}</td>
                                        <td><input  type="text" value="{{ $key['name'] }}" class="name"></td>
                                        <td><input  type="text" value="{{ $key['description'] }}" class="description"></td>
                                        <td><input  type="text" value="{{ $key['cost'] }}" class="cost"></td>
                                        <td><input  type="text" value="{{ $key['price'] }}" class="price"></td>
                                        <td><input  type="text" value="{{ $key['discount'] }}" class="discount"></td>
                                        <td><img src="{{ asset($key['avatar']) }}" style="width:100px" alt=""></td>
                                        <td><input  type="text" value="{{ $key['see_count'] }}" class="see_count"></td>
                                        <td>
                                            <button class="delete-product btn btn-danger">Delete</button>
                                        </td>
                                        <td>
                                            <button class="edit-product btn btn-info">Edit</button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection



