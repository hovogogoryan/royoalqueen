@extends('admin.layouts.app')
@section('content')
    <div class="container content-wrapper">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Add Review</h3>
            </div>
            <form>
                @csrf
                <div class="container">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Name</label>
                        <input type="text" class="form-control" id="review-name" placeholder="name" name="name">
                    </div>


                    <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input type="text" class="form-control" id="review-email" placeholder="email" name="email">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Status</label>
                        <input type="text" class="form-control" id="review-status" placeholder="status" name="status" >
                    </div>


                    <div class="form-group">
                        <label for="exampleInputEmail1">Product_ID</label>
                        <input type="text" class="form-control" id="review-product_id" placeholder="product_id" name="product_id" >
                    </div>
                </div>
                <div class="card-footer">
                    <button type="button" id="add-review" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">

                            <div class="card-body">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>NAME</th>
                                        <th>EMAIL</th>
                                        <th>STATUS</th>
                                        <th>PRODUCT_ID</th>
                                        <th>EDIT</th>
                                        <th>DELETE</th>
                                    </tr>
                                    </thead>
                                    <tbody  id="review-tbody">
                                    @foreach($reviews as $key)
                                        <tr>
                                            <td>{{ $key['id'] }}</td>
                                            <td>{{ $key['name'] }}</td>
                                            <td>{{ $key['email'] }}</td>
                                            <td>{{ $key['status'] }}</td>
                                            <td>{{ $key['product_id'] }}</td>
                                            <td>{{ $key['created_at'] }}</td>
                                            <td>{{ $key['update_at'] }}</td>

                                            <td><button class="delete-review btn btn-danger">Delete</button></td>
                                            <td><button class="edit-review btn btn-danger">Edit</button></td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>

@endsection
