@extends('admin.layouts.app')
@section('content')

    <div class="container content-wrapper">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Add Slider</h3>
            </div>
            <form id="upload_form_slider" enctype="multipart/form-data">
                @csrf
                <div class="container">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Title</label>
                        <input type="text" class="form-control" id="slider-title" name="title" placeholder="title">
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Image</label>
                        <input type="file" class="form-control" id="image-name" name="image" placeholder="image">
                    </div>


                </div>

                <div class="card-footer">
                    <button type="submit" id="add-slider" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">

                            <div class="card-body">
                                <table id="example2" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>TITLE</th>
                                        <th>IMAGE</th>
                                        <th>CREATED_AT</th>
                                        <th>DELETE</th>
                                    </tr>
                                    </thead>

                                    <tbody  id="slider-tbody">
                                        <tfoot>
                                    @foreach($sliders as $key)
                                        <tr>
                                            <td>{{ $key['id'] }}</td>
                                            <td><input  type="text" value="{{ $key['title'] }}" class="name"></td>
                                            <td><img src="{{ asset($key['image']) }}" style="width:100px" alt=""></td>
                                            <td>
                                                <button class="delete-slider btn btn-danger">Delete</button>
                                            </td>
                                            <td>
                                                <button class="edit-slider btn btn-info">Edit</button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </div>

@endsection
