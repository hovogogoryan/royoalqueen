@extends("Layout.app")
@section("content")
<div class="body-wrapper">


    <!-- Utilize Mobile Menu End -->



    <!-- BREADCRUMB AREA START -->
    <div class="ltn__breadcrumb-area ltn__breadcrumb-area-4 ltn__breadcrumb-color-white---">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ltn__breadcrumb-inner text-center">
                        <h1 class="ltn__page-title">Blog</h1>
                        <div class="ltn__breadcrumb-list">
                            <ul>
                                <li><a href="">Home</a></li>
                                <li>Blog</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- BREADCRUMB AREA END -->

    <!-- PAGE DETAILS AREA START (blog-details) -->
    <div class="ltn__page-details-area ltn__blog-details-area mb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="ltn__blog-details-wrap">
                        <div class="ltn__page-details-inner ltn__blog-details-inner">
                            <div class="ltn__blog-meta">
                                <ul>
                                    <li class="ltn__blog-author d-none">
                                        <a href="#">by: Admin</a>
                                    </li>
                                    <li>
                                        <h1> {{$blog->title}}</h1>
                                    </li>
                                    <li class="ltn__blog-comment">
                                        <a href="#"><i class="icon-speech"></i> 2</a>
                                    </li>
                                </ul>
                            </div>
                            <h3 class="ltn__blog-title blog-title-line"><a href="blog-details.html"> </a></h3>
                            <img class="blog-details-main-image mb-15" src="{{asset($blog->image)}}" alt="Image">


                            <div class="row mt-30">
                                <div class="col-sm-6">
                                    <img src="{{asset($blog->image)}}" alt="Image">
                                    <label>Image Caption Here</label>
                                </div>
                                <div class="col-sm-6">
                                    <img src="{{asset($blog->image)}}" alt="Image">
                                </div>
                            </div>
                            <p>{{$blog->content}}</p>
                            <blockquote>
                                Viral dreamcatcher keytar typewriter, aest hetic offal umami. Aesthetic polaroid pug pitchfork post-ironic.
                                <h6 class="ltn__secondary-color m-0 pt-15">Author Name</h6>
                            </blockquote>
                            <p>{{$blog->content}}</p>
                            <p>{{$blog->content}}</p>
                            <img class="alignleft" src="{{asset($blog->image)}}" alt="Image">
                            <p>{{$blog->content}}</p>


                            <h4>{{$blog->title}}</h4>
                            <p>{{$blog->content}}</p>
                        </div>

                        <!-- blog-tags-social-media -->

                        <hr>
                        <!-- prev-next-btn -->
                        <div class="ltn__prev-next-btn row mb-50">
                            <div class="blog-prev col-lg-6">
                                <div class="blog-prev-next-img">
                                    <a href="blog.html"><img src="{{asset($blog->image)}}" alt="Image"></a>
                                </div>
                                <div class="blog-prev-next-info">
                                    <p>Previous Post</p>
                                    <h3 class="ltn__blog-title"><a href="blog.html">{{$blog->title}}</a></h3>
                                </div>
                            </div>
                            <div class="blog-prev blog-next text-right col-lg-6">
                                <div class="blog-prev-next-info">
                                    <p>Next Post</p>
                                    <h3 class="ltn__blog-title"><a href="blog.html"> {{$blog->title}}</a></h3>
                                </div>
                                <div class="blog-prev-next-img">
                                    <a href="blog.html"><img src="{{asset($blog->image)}}" alt="Image"></a>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <!-- comment-area -->
                        <div class="ltn__comment-area mb-50">
                            <div class="ltn-author-introducing clearfix d-none">
                                <div class="author-img">
                                    <img src="{{asset($blog->image)}}" alt="Author Image">
                                </div>
                                <div class="author-info">
                                    <h6>Written by</h6>
                                    <h1>Rosalina D. William</h1>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation is enougn for today.</p>
                                </div>
                            </div>
                            <h4 class="title-2">03 Comments</h4>
                            <div class="ltn__comment-inner">
                                <ul>
                                    <li>
                                        <div class="ltn__comment-item clearfix">
                                            <div class="ltn__commenter-img">
                                                <img src="{{asset($blog->image)}}" alt="Image">
                                            </div>
                                            <div class="ltn__commenter-comment">
                                                <h6><a href="#">{{$blog->title}}</a></h6>
                                                <span class="comment-date">{{$blog->created_at}}</span>
                                                <p>{{$blog->content}}</p>
                                                <a href="#" class="ltn__comment-reply-btn"><i class="icon-reply-1"></i>Reply</a>
                                            </div>
                                        </div>
                                        <ul>


                                </ul>

                            </div>
                        </div>
                        <hr>
                        <!-- comment-reply -->
                        <div class="ltn__comment-reply-area ltn__form-box">
                            <h4 class="title-2">Leave A Comment </h4>
                            <form action="#">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <input type="text" placeholder="Name:">
                                    </div>
                                    <div class="col-lg-4">
                                        <input type="email" placeholder="Email:">
                                    </div>
                                    <div class="col-lg-4">
                                        <input type="text" placeholder="Subject:">
                                    </div>
                                    <div class="col-lg-12">
                                        <textarea placeholder="Type your comments...."></textarea>
                                        <label class="mb-0 input-info-save"><input type="checkbox" name="agree"> Save my name, email, and website in this browser for the next time I comment.</label>
                                        <div class="btn-wrapper">
                                            <button class="btn theme-btn-1 btn-effect-1 text-uppercase" type="submit">Post Comment</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>



                <div class="col-lg-4">
                    <aside class="sidebar-area blog-sidebar ltn__right-sidebar">
                        <!-- Search Widget -->
                        <div class="widget ltn__search-widget">
                            <form action="#">
                                <input type="text" name="search" placeholder="Search">
                                <button type="submit"><i class="fas fa-search"></i></button>
                            </form>
                        </div>
                        <!-- Author Widget -->
                        <div class="widget ltn__author-widget">
                            <div class="ltn__author-widget-inner">
                                <img src="{{asset($blog->image)}}" alt="Image">
                                <h5>Rosalina D. Willaimson</h5>
                                <h6>Head of Company, SEO</h6>
                                <p class="d-none">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis distinctio, odio, eligendi suscipit reprehenderit atque.</p>
                                <div class="ltn__social-media d-none">
                                    <ul>
                                        <li><a href="#" title="Facebook"><i class="icon-social-facebook"></i></a></li>
                                        <li><a href="#" title="Twitter"><i class="icon-social-twitter"></i></a></li>
                                        <li><a href="#" title="Pinterest"><i class="icon-social-pinterest"></i></a></li>
                                        <li><a href="#" title="Instagram"><i class="icon-social-instagram"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- Popular Post Widget -->
                        <div class="widget ltn__popular-post-widget">
                            <h4 class="ltn__widget-title">Recent Post</h4>
                            <ul>
                                <li>
                                    <div class="popular-post-widget-item clearfix">
                                        <div class="popular-post-widget-img">
                                            <a href="blog-details.html"><img src="{{asset($blog->image)}}" alt="#"></a>
                                        </div>
                                        <div class="popular-post-widget-brief">
                                            <div class="ltn__blog-meta">
                                                <ul>
                                                    <li class="ltn__blog-author d-none">
                                                        <a href="#">by: Admin</a>
                                                    </li>
                                                    <li>
                                                        <span> {{$blog->created_at}}</span>
                                                    </li>
                                                    <li class="ltn__blog-comment">
                                                        <a href="#"><i class="icon-speech"></i> 2</a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <h6 class="ltn__blog-title blog-title-line"><a href="blog-details.html">{{$blog->title}}</a></h6>
                                        </div>
                                    </div>
                                </li>




                            </ul>
                        </div>
                        <!-- Category Widget -->
                        <div class="widget ltn__menu-widget">
                            <h4 class="ltn__widget-title">categories</h4>
                            <ul>
                                <li><a href="#">Clothing</a></li>
                                <li><a href="#">Bags</a></li>
                                <li><a href="#">Shoes</a></li>
                                <li><a href="#">Jewelry</a></li>
                                <li><a href="#">Accessories</a></li>
                                <li><a href="#">Food / Drink Store</a></li>
                                <li><a href="#">Gift Store</a></li>
                                <li><a href="#">Accessories</a></li>
                                <li><a href="#">Watch</a></li>
                                <li><a href="#">Uncategorized</a></li>
                                <li><a href="#">Other</a></li>
                            </ul>
                        </div>
                        <!-- Tagcloud Widget -->
                        <div class="widget ltn__tagcloud-widget">
                            <h4 class="ltn__widget-title">Tags</h4>
                            <ul>
                                <li><a href="#">Popular</a></li>
                                <li><a href="#">desgin</a></li>
                                <li><a href="#">ux</a></li>
                                <li><a href="#">usability</a></li>
                                <li><a href="#">develop</a></li>
                                <li><a href="#">icon</a></li>
                                <li><a href="#">Car</a></li>
                                <li><a href="#">Service</a></li>
                                <li><a href="#">Repairs</a></li>
                                <li><a href="#">Auto Parts</a></li>
                                <li><a href="#">Oil</a></li>
                                <li><a href="#">Dealer</a></li>
                                <li><a href="#">Oil Change</a></li>
                                <li><a href="#">Body Color</a></li>
                            </ul>
                        </div>
                        <!-- Social Media Widget -->
                        <div class="widget ltn__social-media-widget d-none">
                            <h4 class="ltn__widget-title ltn__widget-title-border">Never Miss News</h4>
                            <div class="ltn__social-media-2">
                                <ul>
                                    <li><a href="#" title="Facebook"><i class="icon-social-facebook"></i></a></li>
                                    <li><a href="#" title="Twitter"><i class="icon-social-twitter"></i></a></li>
                                    <li><a href="#" title="Pinterest"><i class="icon-social-pinterest"></i></a></li>
                                    <li><a href="#" title="Instagram"><i class="icon-social-instagram"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <!-- Popular Post Widget (Twitter Post) -->
                        <div class="widget ltn__popular-post-widget ltn__twitter-post-widget d-none">
                            <h4 class="ltn__widget-title ltn__widget-title-border">Twitter Feeds</h4>
                            <ul>
                                <li>
                                    <div class="popular-post-widget-item clearfix">
                                        <div class="popular-post-widget-img">
                                            <a href="blog-details.html"><i class="fab fa-twitter"></i></a>
                                        </div>
                                        <div class="popular-post-widget-brief">
                                            <p>Carsafe - #Gutenberg ready
                                                @wordpress
                                                 Theme for Car Service, Auto Parts, Car Dealer available on
                                                @website
                                                <a href="https://website.net">https://website.net</a></p>
                                            <div class="ltn__blog-meta">
                                                <ul>
                                                    <li class="ltn__blog-date">
                                                        <a href="#"><i class="far fa-calendar-alt"></i>June 22, 2020</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="popular-post-widget-item clearfix">
                                        <div class="popular-post-widget-img">
                                            <a href="blog-details.html"><i class="fab fa-twitter"></i></a>
                                        </div>
                                        <div class="popular-post-widget-brief">
                                            <p>Carsafe - #Gutenberg ready
                                                @wordpress
                                                 Theme for Car Service, Auto Parts, Car Dealer available on
                                                @website
                                                <a href="https://website.net">https://website.net</a></p>
                                            <div class="ltn__blog-meta">
                                                <ul>
                                                    <li class="ltn__blog-date">
                                                        <a href="#"><i class="far fa-calendar-alt"></i>June 22, 2020</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="popular-post-widget-item clearfix">
                                        <div class="popular-post-widget-img">
                                            <a href="blog-details.html"><i class="fab fa-twitter"></i></a>
                                        </div>
                                        <div class="popular-post-widget-brief">
                                            <p>Carsafe - #Gutenberg ready
                                                @wordpress
                                                 Theme for Car Service, Auto Parts, Car Dealer available on
                                                @website
                                                <a href="https://website.net">https://website.net</a></p>
                                            <div class="ltn__blog-meta">
                                                <ul>
                                                    <li class="ltn__blog-date">
                                                        <a href="#"><i class="far fa-calendar-alt"></i>June 22, 2020</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <!-- Instagram Widget -->
                        <div class="widget ltn__instagram-widget d-none">
                            <h4 class="ltn__widget-title ltn__widget-title-border">Instagram Feeds</h4>
                            <div class="ltn__instafeed ltn__instafeed-grid insta-grid-gutter"></div>
                        </div>
                        <!-- Banner Widget -->
                        <div class="widget ltn__banner-widget d-none">
                            <a href="shop.html"><img src="img/banner/1.jpg" alt="Banner Image"></a>
                        </div>

                    </aside>
                </div></div>



        </div>
    </div>
    <!-- PAGE DETAILS AREA END -->

    <!-- BRAND LOGO AREA START -->
    <div class="ltn__brand-logo-area  ltn__brand-logo-1 section-bg-1 pt-35 pb-35 plr--5">
        <div class="container-fluid">
            <div class="row ltn__brand-logo-active">
                <div class="col-lg-12">
{{--                    @foreach($brands as $key)--}}
{{--                    <div class="ltn__brand-logo-item">--}}
{{--                        <img src="{{asset('img/brand-logo/1.png')}}" alt="Brand Logo">--}}
{{--                    </div>--}}
{{--                    @endforeach--}}
                </div>





            </div>
        </div>
    </div>
    <!-- BRAND LOGO AREA END -->

    <!-- FOOTER AREA START -->
   @endsection
