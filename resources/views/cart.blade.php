@extends("Layout.app")
@section("content")
<div class="body-wrapper">

    <div class="ltn__utilize-overlay"></div>

    <!-- BREADCRUMB AREA START -->
    <div class="ltn__breadcrumb-area ltn__breadcrumb-area-4 ltn__breadcrumb-color-white---">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="ltn__breadcrumb-inner text-center">
                        <h1 class="ltn__page-title">Cart</h1>
                        <div class="ltn__breadcrumb-list">
                            <ul>
                                <li><a href="index.html">Home</a></li>
                                <li>Cart</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- BREADCRUMB AREA END -->

    <!-- SHOPING CART AREA START -->
    <div class="liton__shoping-cart-area mb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="shoping-cart-inner">
                        <div class="shoping-cart-table table-responsive">
                            <table class="table">
                                <!-- <thead>
                                    <th class="cart-product-remove">Remove</th>
                                    <th class="cart-product-image">Image</th>
                                    <th class="cart-product-info">Product</th>
                                    <th class="cart-product-price">Price</th>
                                    <th class="cart-product-quantity">Quantity</th>
                                    <th class="cart-product-subtotal">Subtotal</th>
                                </thead> -->
                                <tbody>
                                @if(isset($cart_data))
                                    @if(Cookie::get('shopping_cart'))
                                        @php $total="0" @endphp
                                        @foreach($cart_data as $key)
                                          @php $total = $total + ($key["item_quantity"] * $key["item_price"]) @endphp
                                         <tr>
                                        <td class="cart-product-remove"><a href="/remove/cart/{{ $key['item_id'] }}">x</a></td>
                                        <td class="cart-product-image">
                                            <a href="product-details.html"><img src="{{ $key['item_image'] }}" alt="#"></a>
                                        </td>
                                        <td class="cart-product-info">
                                            <h4><a href="product-details.html">{{ $key['item_name'] }}</a></h4>
                                        </td>
                                        <td class="cart-product-price">{{ $key['item_price'] }} AMD</td>
                                        <td class="cart-product-quantity">
                                                <h4>{{ $key['item_quantity'] }}</h4>
                                        </td>
                                        <td class="cart-product-subtotal">{{ $key['item_price']*$key['item_quantity'] }} AMD</td>
                                    </tr>

                                         @endforeach
                                    @endif
                                @else
                                    <div class="row">
                                        <div class="col-md-12 mycard py-5 text-center">
                                            <div class="mycards">
                                                <h4>Your cart is currently empty.</h4>
                                                <a href="{{ url('collections') }}" class="btn btn-upper btn-primary outer-left-xs mt-5">Continue Shopping</a>
                                            </div>
                                        </div>
                                    </div>
                                @endif
{{--                                    <tr class="cart-coupon-row">--}}
{{--                                        <td colspan="6">--}}
{{--                                            <div class="cart-coupon">--}}
{{--                                                <input type="text" name="cart-coupon" placeholder="Coupon code">--}}
{{--                                                <button type="submit" class="btn theme-btn-2 btn-effect-2">Apply Coupon</button>--}}
{{--                                            </div>--}}
{{--                                        </td>--}}
{{--                                        <td>--}}
{{--                                            <button type="submit" class="btn theme-btn-2 btn-effect-2-- disabled">Update Cart</button>--}}
{{--                                        </td>--}}
{{--                                    </tr>--}}
                                </tbody>
                            </table>
                        </div>
                        <div class="shoping-cart-total mt-50">
                            <h4>Cart Totals</h4>
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td>Cart Subtotal</td>
                                        @if(isset($total))
                                        <td>{{ $total }} AMD</td>
                                        @endif

                                    </tr>
                                </tbody>
                            </table>
{{--                            <div class="btn-wrapper text-right">--}}
{{--                                <a href="checkout.html" class="theme-btn-1 btn btn-effect-1">Proceed to checkout</a>--}}
{{--                            </div>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- SHOPING CART AREA END -->

    <!-- BRAND LOGO AREA START -->
    <div class="ltn__brand-logo-area  ltn__brand-logo-1 section-bg-1 pt-35 pb-35 plr--5">
        <div class="container-fluid">
            <div class="row ltn__brand-logo-active">
                <div class="col-lg-12">
                    <div class="ltn__brand-logo-item">
                        <img src="img/brand-logo/1.png" alt="Brand Logo">
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="ltn__brand-logo-item">
                        <img src="img/brand-logo/2.png" alt="Brand Logo">
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="ltn__brand-logo-item">
                        <img src="img/brand-logo/3.png" alt="Brand Logo">
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="ltn__brand-logo-item">
                        <img src="img/brand-logo/4.png" alt="Brand Logo">
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="ltn__brand-logo-item">
                        <img src="img/brand-logo/5.png" alt="Brand Logo">
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="ltn__brand-logo-item">
                        <img src="img/brand-logo/1.png" alt="Brand Logo">
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="ltn__brand-logo-item">
                        <img src="img/brand-logo/2.png" alt="Brand Logo">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- BRAND LOGO AREA END -->

  @endsection
