@extends("Layout.app")
@section('content')




    <div class="ltn__utilize-overlay"></div>

    <!-- SLIDER AREA START (slider-6) -->
    <div class="ltn__slider-area ltn__slider-3 ltn__slider-6 section-bg-1">
        <div class="ltn__slide-one-active slick-slide-arrow-1 slick-slide-dots-1 arrow-white---">
            @foreach($sliders as $key)
                <div class="ltn__slide-item ltn__slide-item-8 text-color-white---- bg-image bg-overlay-theme-black-80---"
                     data-bg="{{ asset($key['image']) }}">
                    <div class="ltn__slide-item-inner">
                        <div class="container">
                            <div class="row">
                                <div class="col-lg-12 align-self-center">
                                    <div class="slide-item-info">
                                        <div class="slide-item-info-inner ltn__slide-animation">
                                            <div class="slide-item-info">
                                                <div class="slide-item-info-inner ltn__slide-animation">
                                                    <h1 class="slide-title animated ">{{ $key->title }}</h1>
                                                    <div class="btn-wrapper animated">
                                                        <a href="service.html" class="theme-btn-1 btn btn-round">Shop
                                                            Now</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <div class="slide-item-img">
                                        <img src="img/slider/41-1.png" alt="#">
                                        <span class="call-to-circle-1"></span>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>

    <div class="ltn__feature-area mt-100 mt--65">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 ">
                    <div class="ltn__feature-item-box-wrap ltn__feature-item-box-wrap-2 ltn__border section-bg-6">
{{--                        <div class="ltn__feature-item ltn__feature-item-8">--}}
{{--                            <div class="ltn__feature-icon">--}}
{{--                                <img src="img/icons/svg/8-trolley.svg" alt="#">--}}
{{--                            </div>--}}
{{--                            <div class="ltn__feature-info">--}}
{{--                                <h4>Free shipping</h4>--}}
{{--                                <p>On all orders over $49.00</p>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="ltn__feature-item ltn__feature-item-8">--}}
{{--                            <div class="ltn__feature-icon">--}}
{{--                                <img src="img/icons/svg/9-money.svg" alt="#">--}}
{{--                            </div>--}}
{{--                            <div class="ltn__feature-info">--}}
{{--                                <h4>15 days returns</h4>--}}
{{--                                <p>Moneyback guarantee</p>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="ltn__feature-item ltn__feature-item-8">--}}
{{--                            <div class="ltn__feature-icon">--}}
{{--                                <img src="img/icons/svg/10-credit-card.svg" alt="#">--}}
{{--                            </div>--}}
{{--                            <div class="ltn__feature-info">--}}
{{--                                <h4>Secure checkout</h4>--}}
{{--                                <p>Protected by Paypal</p>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <div class="ltn__feature-item ltn__feature-item-8">--}}
{{--                            <div class="ltn__feature-icon">--}}
{{--                                <img src="img/icons/svg/11-gift-card.svg" alt="#">--}}
{{--                            </div>--}}
{{--                            <div class="ltn__feature-info">--}}
{{--                                <h4>Offer & gift here</h4>--}}
{{--                                <p>On all orders over</p>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="ltn__banner-area  mt-80">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-4 col-md-6">
                    <div class="ltn__banner-item">
                        <div class="ltn__banner-img">
                            <a href="shop.html"><img src="img/banner/1.jpg" alt="Banner Image"></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="ltn__banner-item">
                        <div class="ltn__banner-img">
                            <a href="shop.html"><img src="img/banner/2.jpg" alt="Banner Image"></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="ltn__banner-item">
                        <div class="ltn__banner-img">
                            <a href="shop.html"><img src="img/banner/3.jpg" alt="Banner Image"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="ltn__product-area ltn__product-gutter  pt-65 pb-40">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title-area text-center">
                        <h1 class="section-title section-title-border">new arrival items</h1>
                    </div>
                </div>
            </div>


            <div class="row justify-content-center">
                @foreach($products as $key)
                    <div class="col-lg-3 col-md-4 col-sm-6 col-6">
                        <div class="ltn__product-item text-center">
                            <div class="product-img">
                                <a href="/product/{{ $key->id }}"><img src="{{asset($key->avatar)}}" alt="#"></a>
                                <div class="product-badge">
                                    <ul>
{{--                                        <li class="badge-2">10%</li>--}}
                                    </ul>
                                </div>
                            </div>
                            <div class="product-info">
                                <h2 class="product-title"><a href="">{{$key->name}}</a></h2>
                                <div class="product-price">
                                    <span>{{$key->price}} AMD</span>
                                    <del>{{$key->discount}} AMD</del>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <!-- PRODUCT SLIDER AREA END -->

    <!-- BANNER AREA START -->
    <div class="ltn__banner-area ">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="ltn__banner-item">
                        <div class="ltn__banner-img">
                            <a href="#"><img src="img/banner/6.jpg" alt="Banner Image"></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="ltn__banner-item">
                        <div class="ltn__banner-img">
                            <a href="#"><img src="img/banner/7.jpg" alt="Banner Image"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- BANNER AREA END -->

    <!-- PRODUCT SLIDER AREA START -->
    <div class="ltn__product-slider-area ltn__product-gutter  pt-60 pb-40">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title-area text-center">
                        <h1 class="section-title section-title-border">top products</h1>
                    </div>
                </div>
            </div>
            <div class="row ltn__product-slider-item-four-active slick-arrow-1">
                @foreach($productsTop as $key)
                    <div class="col-12">
                        <div class="ltn__product-item text-center">
                            <div class="product-img">
                                <a href="/product/{{$key->id}}"><img src="{{ asset($key->avatar) }}" alt="#"></a>
                                <div class="product-badge">
                                    <ul>
{{--                                        <li class="badge-2">10%</li>--}}
                                    </ul>
                                </div>
                            </div>
                            <div class="product-info">
                                <h2 class="product-title"><a href="/product/{{ $key->id }}">{{$key->name}}</a></h2>
                                <div class="product-price">
                                    <span>{{ $key->price }} AMD</span>
                                    <del>{{$key->discount}} AMD</del>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <!-- PRODUCT SLIDER AREA END -->

        <!-- BANNER AREA START -->
        <div class="ltn__banner-area ">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="ltn__banner-item">
                            <div class="ltn__banner-img">
                                <a href="#"><img src="img/banner/10.jpg" alt="Banner Image"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- BANNER AREA END -->

        <!-- BLOG AREA START (blog-3) -->
        <div class="ltn__blog-area  pt-60 pb-30">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-title-area text-center">
                            <h1 class="section-title section-title-border">latest news</h1>
                        </div>
                    </div>
                </div>
                <div class="row  ltn__blog-slider-one-active slick-arrow-1">
                    <!-- Blog Item -->
                    @foreach($blogs as $key)
                    <div class="col-lg-12">
                        <h1>{{$key->title}}</h1>
                        <div class="ltn__blog-item">
                            <div class="ltn__blog-img">
                                <a href="/blog/{{$key->id}}"><img src="{{asset($key->image)}}" alt="#" width="300px" height="200px" style="object-fit: cover"></a>
                            </div>
                            <div class="ltn__blog-brief">
                                <div class="ltn__blog-meta">
                                    <ul>
                                        <li class="ltn__blog-author d-none">
                                            <a href="#">by: Admin</a>
                                        </li>
                                        <li>
                                            <span>{{$key->created_at}}</span>
                                        </li>
                                        <li class="ltn__blog-comment">
                                            <a href="#"><i class="icon-speech"></i> 2</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

                    <!--  -->
                </div>
            </div>
        </div>

        <!-- BRAND LOGO AREA START -->
        <div class="ltn__Fnd-logo-area  ltn__brand-logo-1 section-bg-1 pt-35 pb-35 plr--5">
            <div class="container-fluid">
                <div class="row ltn__brand-logo-active">

                    @foreach($brands as $key)
                    <div class="col-lg-12">
                        <div class="ltn__brand-logo-item">
                            <img src="{{asset($key->image)}}" alt="Brand Logo">
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
        </div>
@endsection
