<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::get('/home', 'IndexController@index');
Route::get('about', 'AboutController@index');
Route::get('blog', 'BlogController@index');
Route::get('cart', 'CartController@index');
Route::get('checkout', 'checkoutController@index');
Route::get('contact', 'ContactController@index');
Route::get('login', 'LoginController@index');
Route::get('portfolio', 'PortfolioController@index');
Route::get('register', 'RegisterController@index');
Route::get('shop', 'ShopController@index');
Route::get('wishlist', 'WishlistController@index');
Route::get('product/{id}', 'ProductController@show');
Route::get('blog/{id}', 'BlogController@show');
Route::get('/search', 'SearchController@store');
Route::get('/filter', 'SearchController@filter');

//Route::get('/product', 'ProductController@index');
Route::get('/category/{id}', 'admin\CategoryController@show');


Route::prefix('admin')->middleware('auth')->group(function () {
    Route::get('', 'admin\DashboardController@index');

    Route::prefix('product')->group(function(){
        Route::get('', 'admin\ProductController@index');
        Route::post('store', 'admin\ProductController@store');
        Route::get('delete/{id}', 'admin\ProductController@destroy');
        Route::post('update/{id}', 'admin\ProductController@update');
    });

    Route::post('category/update/{id}', 'admin\CategoryController@update');
    Route::get('category', 'admin\CategoryController@index');
    Route::post('category/store', 'admin\CategoryController@store');
    Route::get('category/delete/{id}', 'admin\CategoryController@destroy');


    Route::post('slider/update/{id}', 'admin\SliderController@update');
    Route::get('slider', 'admin\SliderController@index');
    Route::post('slider/store', 'admin\SliderController@store');
    Route::get('slider/delete/{id}', 'admin\SliderController@destroy');

    Route::post('blog/update/{id}', 'admin\BlogController@update');
    Route::get('blog', 'admin\BlogController@index');
    Route::post('blog/store', 'admin\BlogController@store');
    Route::get('blog/delete/{id}', 'admin\BlogController@destroy');

    Route::post('brand/update/{id}', 'admin\BrandController@update');
    Route::get('brand', 'admin\BrandController@index');
    Route::post('brand/store', 'admin\BrandController@store');
    Route::get('brand/delete/{id}', 'admin\BrandController@destroy');


    Route::get('review/', 'admin\ReviewController@index');
    Route::post('review/store', 'admin\ReviewController@store');

});


Route::post('/add/cart/{id}', 'CartController@store');
Route::get('/remove/cart/{id}', 'CartController@destroy');


Auth::routes();

